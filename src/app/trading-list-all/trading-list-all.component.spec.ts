import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingListAllComponent } from './trading-list-all.component';

describe('TradingListAllComponent', () => {
  let component: TradingListAllComponent;
  let fixture: ComponentFixture<TradingListAllComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradingListAllComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingListAllComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
