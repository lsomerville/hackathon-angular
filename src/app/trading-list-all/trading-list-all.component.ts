import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { RestApiService } from "../shared/rest-api.service";
import { Order } from "../shared/order"

@Component({
  selector: 'app-trading-list-all',
  templateUrl: './trading-list-all.component.html',
  styleUrls: ['./trading-list-all.component.css']
})
export class TradingListAllComponent implements OnInit {

  Orders: Order[] = [];
  constructor(
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadOrders()
  }

  loadOrders() {
    return this.restApi.getOrders().subscribe((data: Order[]) => {
        this.Orders = data;
        console.log(this.Orders[0].createdTimestamp);
    })
  }

  deleteOrder(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteOrder(id).subscribe(data => {
        this.loadOrders()
      })
    }
  }

}
