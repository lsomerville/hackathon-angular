import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingNewComponent } from './trading-new.component';

describe('TradingNewComponent', () => {
  let component: TradingNewComponent;
  let fixture: ComponentFixture<TradingNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradingNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
