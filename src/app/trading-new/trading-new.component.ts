import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';


@Component({
  selector: 'app-trading-new',
  templateUrl: './trading-new.component.html',
  styleUrls: ['./trading-new.component.css']
})
export class TradingNewComponent implements OnInit {

  initDate = new Date();

  @Input() orderDetails = { id: 0, stockTicker: '', price: 0, volume:0,
                            buyOrSell:'', statusCode:0, createdTimestamp: this.initDate}

  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  addOrder(){
    this.restApi.createOrder(this.orderDetails).subscribe((data:{}) => {
      this.router.navigate(['/trading-list-all'])
    })
  }

}
