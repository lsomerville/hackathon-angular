import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TradingNewComponent } from './trading-new/trading-new.component';
import { TradingEditComponent } from './trading-edit/trading-edit.component';
import { TradingListAllComponent } from './trading-list-all/trading-list-all.component';
import { FormsModule } from '@angular/forms';
import { TradingDebugComponent } from './trading-debug/trading-debug.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

@NgModule({
  declarations: [
    AppComponent,
    TradingNewComponent,
    TradingEditComponent,
    TradingListAllComponent,
    TradingDebugComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
