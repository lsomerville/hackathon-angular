export class Order {
  constructor(){
    this.id=0;
    this.stockTicker="";
    this.price = 0;
    this.buyOrSell = "";
    this.volume = 0;
    this.statusCode = 0;
    this.createdTimestamp = new Date();
}
id: number;
stockTicker: string;
price: number;
buyOrSell: string;
volume: number;
statusCode: number;
createdTimestamp: Date;
}

