import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-trading-edit',
  templateUrl: './trading-edit.component.html',
  styleUrls: ['./trading-edit.component.css']
})
export class TradingEditComponent implements OnInit {


  id = this.actRoute.snapshot.params['id'];
  orderDetails: any = {};
  constructor(public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router) { }

  ngOnInit(): void {
    this.restApi.getOrder(this.id).subscribe((data: {}) => {
      this.orderDetails = data;
    })
  }
  updateOrder() {
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateOrder(this.id, this.orderDetails).subscribe(data => {
        this.router.navigate(['/trading-debug'])
      })
    }
  }
}

