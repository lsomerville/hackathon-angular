import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { RestApiService } from "../shared/rest-api.service";
import { Order } from "../shared/order"

@Component({
  selector: 'app-trading-debug',
  templateUrl: './trading-debug.component.html',
  styleUrls: ['./trading-debug.component.css']
})
export class TradingDebugComponent implements OnInit {

  Orders: Order[] = [];
  constructor(
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadOrders()
  }

  loadOrders() {
    return this.restApi.getOrders().subscribe((data: Order[]) => {
        this.Orders = data;
        console.log(this.Orders[0].createdTimestamp);
    })
  }

  deleteOrder(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteOrder(id).subscribe(data => {
        this.loadOrders()
      })
    }
  }

}

