import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingDebugComponent } from './trading-debug.component';

describe('TradingDebugComponent', () => {
  let component: TradingDebugComponent;
  let fixture: ComponentFixture<TradingDebugComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TradingDebugComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingDebugComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
