import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TradingDebugComponent } from './trading-debug/trading-debug.component';
import { TradingEditComponent } from './trading-edit/trading-edit.component';
import { TradingListAllComponent } from './trading-list-all/trading-list-all.component';
import { TradingNewComponent } from './trading-new/trading-new.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'trading-list-all' },
  { path: 'trading-new', component: TradingNewComponent },
  { path: 'trading-list-all', component: TradingListAllComponent },
  { path: 'trading-edit/:id', component: TradingEditComponent },
  { path: 'trading-debug', component: TradingDebugComponent },
  { path: '**', component: TradingListAllComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
